/*
 * camera.h
 *
 *  Created on: Jun 27, 2014
 *      Author: saman
 */

#ifndef CAMERA_H_
#define CAMERA_H_

#define GAIN    0x00
#define BGAIN   0x01
#define RGAIN   0x02
#define GGAIN   0x03

#define PWC0	0x49
#define REG3E	0x3E
#define PLL		0x29
#define REG12	0x12
#define CLKRC	0x11
#define REG0C	0x0C
#define REG0E	0x0E
#define REG15	0x15
#define REG16	0x16
#define HSTART	0x17
#define HSIZE	0x18
#define VSTART	0x19
#define VSIZE	0x1A

#define REGB4	0xB4	// Sharpenning - Denoising auto/manual mode selection.
#define REGB6	0xB6	// Sharpenning magnitude adjustment.

#define REGC8	0xC8
#define REGC9	0xC9
#define REGCA	0xCA
#define REGCB	0xCB

#define REGCC	0xCC
#define REGCD	0xCD
#define REGCE	0xCE
#define REGCF	0xCF


void camera_init(void);
void camera_data_fetch(void);

#endif /* CAMERA_H_ */
