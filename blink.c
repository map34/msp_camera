//***************************************************************************************
/*
Lincong Li
This code has a working SMCLK, all interrupt, I2C write

Connection:
3.3V
SI0C: pin32
VSYNC: pin38 (P2.7) pin26 (P2.2)
PCLK: pin1 (DMAE0)

GND
SI0D: pin31
HREF:
XCLK: pin27 (SMCLK)

D0~D7: port4.0 ~ port4.7

*/
//***************************************************************************************



#include <msp430.h>
#include "driverlib.h"
#include "camera.h"
//#include "pin-assign.h"
#define DMA_NUM 25344  //176*144
//#define DMA_NUM 10
#define DMA_SOURCE_ADDR 0x0221 //address of port 4 in
#define DMA_DEST_ADDR 0x5000 //address of FRAM
//void send_Frame_To_PC(void*, int);

void flash_LED()
{
	volatile unsigned int i;            // volatile to prevent optimization
    P1OUT ^= 0x01;                      // Toggle P1.0 using exclusive-OR
    i = 1000000;                          // SW Delay
    //i = 1000000;
    do i--;
    while(i != 0);
}

//Fault ISR
void error_LED()
{
	while(1){
		flash_LED();
	}
}


void i2cWriteToReg(uint8_t byteAddr, uint8_t data)
{

	UCB0I2CSA = 0x21;//camAddr_W; 								// set slave address
	while(UCB0CTLW0 & UCTXSTP);
	UCB0CTLW0 |= UCTR | UCTXSTT;							// put in transmitter mode and send start bit

	while(UCB0CTLW0 & UCTXSTT);
	while (!(UCB0IFG & UCTXIFG0)); 							// wait for previous tx to complete

	//UCB0TXBUF = TxBuff;	 // setting TXBUF clears the TXIFG flag
	UCB0TXBUF = byteAddr;
	while (!(UCB0IFG & UCTXIFG0));
	//__delay_cycles(200); //try

	//TxBuff = data;
	//UCB0TXBUF = TxBuff; 										// setting TXBUF clears the TXIFG flag
	UCB0TXBUF = data;
	while (!(UCB0IFG & UCTXIFG0)); 							// wait for previous tx to complete


	UCB0CTLW0 |= UCTXSTP; 									// I2C stop condition
	while (UCB0CTLW0 & UCTXSTP);
}

//send SA, send byteAddr, recieve data,
uint8_t i2cReadFromReg(uint8_t byteAddr)
{
	uint8_t data = 0;

	/* **** WRITING TO CAMERA WRITE ADDRESS TO LOOK FOR REG **** */
	UCB0I2CSA = 0x21;//camAddr_W; 									// set slave address

	UCB0CTLW0 |= UCTR | UCTXSTT;							// put in transmitter mode and send start bit
	while (!(UCB0IFG & UCTXIFG0) | (UCB0CTLW0 & UCTXSTT)); 	// wait for previous tx to complete

	UCB0TXBUF = byteAddr; 									// setting TXBUF clears the TXIFG flag
	while (!(UCB0IFG & UCTXIFG0) | (UCB0CTLW0 & UCTXSTT)); 	// wait for previous tx to complete

	UCB0CTLW0 |= UCTXSTP;
	while (UCB0CTLW0 & UCTXSTP);


	/* **** WRITING TO CAMERA READ ADDRESS TO RECEIVE FROM REG **** */

	UCB0CTLW0 &= ~UCTR;										//change to receiver mode

	UCB0CTLW0 |= UCTXSTT;									// send start bit

	while (!(UCB0IFG & UCRXIFG0));							// wait for receive flag

	UCB0CTLW0 |= UCTXSTP; 									// I2C stop condition

	data = UCB0RXBUF;										// get the data

	while (UCB0CTLW0 & UCTXSTP);							// wait until stopping

	return data;
}

void i2c_init()
{
	__delay_cycles(200000);
	P1SEL1 |= BIT6 | BIT7;                         			// Assign I2C pins to SCL/SDA
	//P1SEL0 &= ~(BIT6 | BIT7);								// Selection mode P1SEL1=1 and P1SEL0=0, so mode 0b10 => SCL/SDA

	UCB0CTLW0 |= UCSWRST;                  				// Enable SW reset (reset state machine)
	//UCB0CTL1 |= UCSWRST;   //from datasheet
	UCB0CTLW0 |= (UCMST | UCMODE_3 | UCSYNC);     						// I2C Master, synchronous mode
	UCB0CTLW0 |= (UCSSEL__SMCLK | UCTR);            						// Use SMCLK, keep SW reset
	//UCB0BRW = 0x1E;
	// fSCL = SMCLK/120, for example
	UCB0BRW = 120;
	//UCB0BRW = 0x3C; // 60
	//UCB0BRW = 0x1E; //30
	UCB0CTLW0 &= (~UCSWRST);                    				// Clear SW reset, resume operation
	//UCB0CTL1 =  &= ~UCSWRST;
	 __delay_cycles(5000000);
}


void camera_init(){
    //BITCLR(PIN_PWDN_OUT , PIN_PWDN); // Making sure that the camera is not in the PWDN mode.
	//BITCLR((P1OUT) , (BIT5)); // Making sure that the camera is not in the PWDN mode.
	//PCLK settings 6 MHz for 24MHz input clock.
    // It is possible to increase the clock up to 8MHz but needs some post computation to extract the correct image.
//    i2c_write(REG_COM14 , 0x08);	//Enable PCLK scaling.

	//PCLK settings 6 MHz for 24MHz input clock.
    // It is possible to increase the clock up to 8MHz but needs some post computation to extract the correct image.
    i2cWriteToReg(REG_COM14 , 0x08);	//Enable PCLK scaling.
    //__delay_cycles(200000);
    i2cWriteToReg(REG_CLKRC , 0x81);	// Devide by 2
	//i2cWriteToReg(REG_CLKRC , 0x81); //prescale the pclk a little
	//__delay_cycles(200000);
//	i2c_write(REG_DBLV , 0x7A);		// Multiply by 4

	//Frame per second settings
	i2cWriteToReg(REG_COM3 , COM3_SCALEEN);
	//__delay_cycles(200000);

	i2cWriteToReg(REG_COM7 , COM7_FMT_QCIF | COM7_YUV);// QCIF frame resolution is selected. Also YUV output format is set.
	//__delay_cycles(200000);

	i2cWriteToReg(REG_COM10 , COM10_PCLK_HB);	// Suppress PCLK on horiz blank. PCLK close doesn't not run during the horizontal blank
	//__delay_cycles(200000);

	i2cWriteToReg(REG_GFIX , 0xFF);  // Gain control: 1.75x for all Gb, Gr, R and B channels.
	//__delay_cycles(200000);

	i2cWriteToReg(REG_REG74 , 0x13);
	//__delay_cycles(200000);
    //BITSET(PIN_PWDN_OUT , PIN_PWDN); // Setting camera in PWDN mode.

}

uint8_t val1 = 0;
uint8_t val2 = 0;
uint8_t val3 = 0;
uint8_t val4 = 0;
uint8_t val5 = 0;
//read register data back from the camera
void check_Camera_Reg_Values()
{
	val1 = i2cReadFromReg(REG_COM14); //0x08
	UART_send_Byte(val1);
	__delay_cycles(10000);
	//UART_send_Byte('\n');
	val2 = i2cReadFromReg(REG_COM3); //0x08
	//UART_send_Byte(val2);
	__delay_cycles(10000);
	//UART_send_Byte('\n');
	val3 = i2cReadFromReg(REG_CLKRC); //0x81
	//UART_send_Byte(val3);
	__delay_cycles(10000);
	//UART_send_Byte('\n');
	val4 = i2cReadFromReg(REG_COM7); //0b00001000
	//UART_send_Byte(val4);
	__delay_cycles(10000);
	//UART_send_Byte('\n');
	val5 = i2cReadFromReg(REG_COM10); //0x20
	//UART_send_Byte(val5);
	__delay_cycles(10000);
	//UART_send_Byte('\n');
}

void camera_init_test(){
	i2cWriteToReg(REG_COM7, 0x80); //reset all registers
	__delay_cycles(100000);

    i2cWriteToReg(REG_COM3 , COM3_SCALEEN); //enable manual adjust
    __delay_cycles(100);

    i2cWriteToReg(REG_CLKRC, 0x80);	// Devide by 1  0x80 is the default (no scaling)
    __delay_cycles(100);

    i2cWriteToReg(REG_COM7 , COM7_FMT_QCIF);// QCIF frame resolution is selected. Also YUV output format is set.
    __delay_cycles(100);

	i2cWriteToReg(REG_COM10 , COM10_PCLK_HB);	// Suppress PCLK on horiz blank. PCLK close doesn't not run during the horizontal blank
	__delay_cycles(100);

	i2cWriteToReg(REG_GFIX , 0xFF);  // Gain control: 1.75x for all Gb, Gr, R and B channels.
	__delay_cycles(100);

	i2cWriteToReg(REG_REG74 , 0x13); //digital gain control
	__delay_cycles(100);
	//check_Camera_Reg_Values();
}
/*freq: 24 ==> 24MHz
  freq: 16 ==> 16MHz
  freq: 8 ==> 8MHz
*/
void DCO_Conf(int freq)
{
    //SET CLOCK SURCE
    //CSCTL0_H = CSKEY;
    CSCTL0_H = 0xA5;       // Unlock CS registers
    switch(freq){
        case 24:
            CSCTL1_L = (DCOFSEL_6 | DCORSEL); //set DCO frequency 24 MHz
            FRCTL0 = 0xA500 | ((1) << 4);  //FRCTLPW | NWAITS_1;
            break;
        case 8:
            CSCTL1_L = DCOFSEL_6;
            CSCTL1_L &= ~(DCORSEL); //set DCO frequency 8 MHz
            break;
        case 16:
            CSCTL1_L = DCOFSEL_4; //DCOFSEL_4 == 0x8
            CSCTL1_L |= DCORSEL; //set DCO frequency 16 MHz
            FRCTL0 = 0xA500 | ((1) << 4);  //FRCTLPW | NWAITS_1;
            break;
    }
    CSCTL2_L = (0b0110011); //set the source of MCLK and SMCLK to be DCO
    CSCTL3_L = DIVS__1;        //SMCLK/1 and MCLK/1 Page:
    //CSCTL3_L = 0x0010; //SMCLK/2 and MCLK/1
    //CSCTL3_L = 0x0020; //SMCLK/4 and MCLK/1

    CSCTL4_L &= 0b01;    //SMCLK ON

    //SET I/O port Pin27 port3.4
    //try 10b ==> secondeary function
    P3DIR |= 0x010; // Set P3.4 to output direction

    //try tertiary
    P3SEL0 |= 0x0010; //P3SEL0.4 = 1
    P3SEL1 |= 0x0010;//P3SEL1.4 = 0
}

//Sample code from CSSware
void DCO_Lib_Conf()
{
    //Set DCO Frequency to 8MHz
    CS_setDCOFreq(CS_DCORSEL_0,CS_DCOFSEL_6);
    //CS_setDCOFreq(CS_DCORSEL_1, CS_DCOFSEL_4);
    //configure MCLK, SMCLK to be source by DCOCLK
    CS_initClockSignal(CS_SMCLK,CS_DCOCLK_SELECT,CS_CLOCK_DIVIDER_1);
    CS_initClockSignal(CS_MCLK,CS_DCOCLK_SELECT,CS_CLOCK_DIVIDER_1);

    // output SMCLK
    GPIO_setOutputLowOnPin(
        GPIO_PORT_P3,
        GPIO_PIN4
        );

    GPIO_setAsPeripheralModuleFunctionOutputPin(
        GPIO_PORT_P3,
        GPIO_PIN4,
        GPIO_TERNARY_MODULE_FUNCTION
        );

    /*
     * Disable the GPIO power-on default high-impedance mode to activate
     * previously configured port settings
     */
    //PMM_unlockLPM5();
}

//configure interrupt on port1, pin 4, hight-to-low trigger
void P1_4_interrupt_Conf()
{
    //Stop watchdog timer
    //WDT_A_hold(WDT_A_BASE);

    //Set P1.0 to output direction
    GPIO_setAsOutputPin(
        GPIO_PORT_P1,
        GPIO_PIN0
        );

    //Enable P1.4 internal resistance as pull-Up resistance
    GPIO_setAsInputPinWithPullUpResistor(
        GPIO_PORT_P1,
        GPIO_PIN4
        );

    //P1.4 interrupt enabled
    GPIO_enableInterrupt(
        GPIO_PORT_P1,
        GPIO_PIN4
        );

    //P1.4 Hi/Lo edge
    GPIO_selectInterruptEdge(
        GPIO_PORT_P1,
        GPIO_PIN4,
        //GPIO_HIGH_TO_LOW_TRANSITION
		GPIO_LOW_TO_HIGH_TRANSITION
        );

    //P1.4 IFG cleared
    GPIO_clearInterrupt(
        GPIO_PORT_P1,
        GPIO_PIN4
        );

}


//configure interrupt on port1, pin 5, hight-to-low trigger
void P1_5_interrupt_Conf()
{
    //Enable P1.5 internal resistance as pull-Up resistance
    GPIO_setAsInputPinWithPullUpResistor(
        GPIO_PORT_P1,
        GPIO_PIN5
        );

    //P1.5 interrupt enabled
    GPIO_enableInterrupt(
        GPIO_PORT_P1,
        GPIO_PIN5
        );

    //P1.5 Hi/Lo edge
    GPIO_selectInterruptEdge(
        GPIO_PORT_P1,
        GPIO_PIN5,
        //GPIO_HIGH_TO_LOW_TRANSITION
		GPIO_LOW_TO_HIGH_TRANSITION
        );

    //P1.5 IFG cleared
    GPIO_clearInterrupt(
        GPIO_PORT_P1,
        GPIO_PIN5
        );
}

//configure interrupt on port2, pin1, hight-to-low trigger
void P2_7_interrupt_Conf()
{
    //Enable P2.7 internal resistance as pull-Up resistance
    GPIO_setAsInputPinWithPullUpResistor(
        GPIO_PORT_P2,
        GPIO_PIN7
        );

    //P2.1 interrupt enabled
    GPIO_enableInterrupt(
        GPIO_PORT_P2,
        GPIO_PIN7
        );

    //P2.1 Hi/Lo edge
    GPIO_selectInterruptEdge(
        GPIO_PORT_P2,
        GPIO_PIN7,
        //GPIO_HIGH_TO_LOW_TRANSITION
		GPIO_LOW_TO_HIGH_TRANSITION
        );

    //P2.1 IFG cleared
    GPIO_clearInterrupt(
        GPIO_PORT_P2,
        GPIO_PIN7
        );
}

//configure interrupt on port2, pin2, low-to-high trigger
void P2_2_interrupt_Conf()
{
    //Enable P2.2 internal resistance as pull-Up resistance
    GPIO_setAsInputPinWithPullUpResistor(
        GPIO_PORT_P2,
        GPIO_PIN2
        );

    //P2.2 interrupt enabled
    GPIO_enableInterrupt(
        GPIO_PORT_P2,
        GPIO_PIN2
        );

    //P2.2 Hi/Lo edge
    GPIO_selectInterruptEdge(
        GPIO_PORT_P2,
        GPIO_PIN2,
        //GPIO_HIGH_TO_LOW_TRANSITION
		GPIO_LOW_TO_HIGH_TRANSITION
        );

    //P2.2 IFG cleared
    GPIO_clearInterrupt(
        GPIO_PORT_P2,
        GPIO_PIN2
        );
}

//configure interrupt on port2, pin2, low-to-high trigger
void P3_6_interrupt_Conf()
{
    //Enable P2.1 internal resistance as pull-Up resistance
    GPIO_setAsInputPinWithPullUpResistor(
        GPIO_PORT_P3,
        GPIO_PIN6
        );

    //P2.1 interrupt enabled
    GPIO_enableInterrupt(
        GPIO_PORT_P3,
        GPIO_PIN6
        );

    //P2.1 Hi/Lo edge
    GPIO_selectInterruptEdge(
        GPIO_PORT_P3,
        GPIO_PIN6,
        //GPIO_HIGH_TO_LOW_TRANSITION
		GPIO_LOW_TO_HIGH_TRANSITION
        );

    //P2.1 IFG cleared
    GPIO_clearInterrupt(
        GPIO_PORT_P3,
        GPIO_PIN6
        );
}

void enable_All_Interrupts()
{
    //P1_4_interrupt_Conf();
    P1_5_interrupt_Conf();
    //P2_7_interrupt_Conf(); //VSYNC H to L
    //P2_2_interrupt_Conf(); //VSYNC L to H
    //P3_6_interrupt_Conf();
}

void disable_All_Interrupts()
{
	GPIO_disableInterrupt(
	        GPIO_PORT_P3,
	        GPIO_PIN6
	);

	GPIO_disableInterrupt(
	        GPIO_PORT_P1,
	        GPIO_PIN4
	);

	GPIO_disableInterrupt(
	        GPIO_PORT_P1,
	        GPIO_PIN5
	);

	GPIO_disableInterrupt(
	        GPIO_PORT_P2,
	        GPIO_PIN7
	);

	GPIO_disableInterrupt(
	        GPIO_PORT_P2,
	        GPIO_PIN2
	);
}

void GPIO_conf()
{
	//P1DIR |= 0x01;                          // Set P1.0 to output direction
	/*
	 * P3.0 ==> rising edge of HREF
	 * P3.1 ==> falling edge of HREF
	 * P3.2 ==> rising edge of VSYNC
	 * P3.3 ==> falling edge of VSYN
	 */
	P3DIR |= 0x01; //Set P3.0 to output direction
	P3DIR |= 0x02; //Set P3.1 to output direction
	P3DIR |= 0x04; //Set P3.2 to output direction
	P3DIR |= 0x08; //Set P3.3 to output direction
}

void GPIOP4_conf()
{

	P4DIR &= ~(0xFF); //port 4 all as input
}

void general_Init()
{
    WDTCTL = WDTPW | WDTHOLD;               // Stop watchdog timer
    PM5CTL0 &= ~LOCKLPM5;                   // Disable the GPIO power-on default high-impedance mode
                                            // to activate previously configured port settings
	//P1SEL1 |= BIT0;
	//P1SEL0 &= ~(BIT0); //pinmux p1.0 to be DMA trigger pin
}

//useful URL: https://e2e.ti.com/support/microcontrollers/msp430/f/166/t/335859
//source addr: P4IN
//destination addr: FRAM starting at
//Transfer mode: Single Transfer
//Address mode: FA -> BA
void DMA_Conf()
{

	  //P1SEL1 |= BIT0;
	  //P1SEL0 &= ~(BIT0); //pinmux p1.0 to be DMA trigger pin

	 // P1SEL1 |= BIT0;
	 // P1SEL0 &= ~(BIT0); //pinmux p1.0 to be DMA trigger pin
	  // Configure DMA channel 0
	  //__data16_write_addr((unsigned short) &DMA0SA,(unsigned long) 0x1C20);
	  __data16_write_addr((unsigned short) &DMA0SA, DMA_SOURCE_ADDR);

	                                            // Source block address
	  //__data16_write_addr((unsigned short) &DMA0DA,(unsigned long) 0x1C40);
	  __data16_write_addr((unsigned short) &DMA0DA, DMA_DEST_ADDR);
	                                            // Destination single address
	  DMA0SZ = DMA_NUM;                              // Block size
	  //DMA0CTL
	  DMA0CTL = DMADT_0 | DMASRCINCR_0 | DMADSTINCR_3 | DMASRCBYTE | DMADSTBYTE; // Rpt, inc

	  //DMA_TRIGGERSOURCE_26
	  //DMAIV |= 0x02;

	  DMACTL0 |= DMA0TSEL_31;//0x01; //try trigger source 0

	  //set edge-sensitive inerrupt
//	  DMA0CTL &= ~(DMALEVEL); //edge sensitive
	  //DMA0CTL |= DMAIE; //interrupt enable
	  //DMA0CTL |= DMAEN;                         // Enable DMA0
}



void UART_Conf(){
	  // Configure GPIO
	  P2SEL1 |= BIT0 | BIT1;                    // USCI_A0 UART operation
	  P2SEL0 &= ~(BIT0 | BIT1);

	  // Configure USCI_A0 for UART mode
	  UCA0CTLW0 = UCSWRST;                      // Put eUSCI in reset
	  UCA0CTLW0 |= UCSSEL__SMCLK;               // CLK = SMCLK

	  //UCA0BR0 = 52;                             // 8000000/16/9600
	  UCA0BR0 = 78;//156;                             // 12000000/16/9600
	  UCA0BR1 = 0x00;
	  UCA0MCTLW |= UCOS16 | UCBRF_1;
	  UCA0CTLW0 &= ~UCSWRST;                    // Initialize eUSCI
}


void UART_send_Byte(uint8_t byte)//unsigned char byte)
{
	//UCA0TXBUF = byte;
	//__delay_cycles(4000);
	while( !(UCTXIFG & UCA0IFG) );
	UCA0TXBUF = byte;
}

void UART_self_Check()
{
	UART_send_Byte('R');
	UART_send_Byte('e');
	UART_send_Byte('a');
	UART_send_Byte('d');
	UART_send_Byte('y');
	UART_send_Byte('\n');
}

void send_Frame_To_PC( uint8_t* start_Addr, int byte_Num)
{
	int i;
	//byte_Num = byte_Num*2;
	for(i=0; i<byte_Num; i++){
		UART_send_Byte( *(start_Addr+i) );
	}
}



//global variables
//char line_Start = 0;
//char frame_Start = 0;
//char finished_A_Frame = 0;
//int line_Num=0;
//int each_line_bytes_Num=0;
int data = 0;
//int frame_Num = 0;
int frame_Start = 0;
int frame_Finished = 0;
//int frame_Finished_2 = 0;
int frame_Count=0;
//main--------------------------------------------------------------
//------------------------------------------------------------------
//------------------------------------------------------------------
int main(void) {

	general_Init(); //disable the LM5 mode and watch dog timer
	GPIOP4_conf();
	P3DIR |= 0x04; // Set P3.2 to output direction
	P3OUT &= ~(0x04); //initially low
	__delay_cycles(1000);
    DCO_Conf(24); //configure the clock to MCLK to be 24MHz
    i2c_init();
    __delay_cycles(100);
    camera_init_test();
    //UART_self_Check();
	P1SEL1 |= BIT0;
	P1SEL0 &= ~(BIT0); //pinmux p1.0 to be DMA trigger pin
	UART_Conf();
	DMA_Conf(); //reinitialize DMA channel 0 and reload the DMA0SZ register
    enable_All_Interrupts(); //enable all 5 interrupts
    __bis_SR_register(GIE);
    while(1){

    		if(frame_Finished == 5){
    			//since we can just save a segment of memory, so no need to use UART
    			//send_Frame_To_PC(DMA_DEST_ADDR, DMA_NUM);
    			frame_Finished = 0;
    		}
    }
    return 0;
}


//******************************************************************************
//
//This is the PORT1_VECTOR interrupt vector service routine
//
//******************************************************************************
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=PORT1_VECTOR
__interrupt
#elif defined(__GNUC__)
__attribute__((interrupt(PORT1_VECTOR)))
#endif
void Port_1(void)
{
	//port 2.7 H to L
	//if(frame_Finished == 0){
		//if( ((P2IN >> 7) & 1) == 0){ //currently Low (High to Low) falling edge of VSYNC
	if(frame_Finished == 4){
		//disable interrupt
		P3OUT |= 0x04;
		DMA0CTL |= DMAIE; //interrupt enable
		DMA0CTL |= DMAEN;                         // Enable DMA0
		frame_Start = 1;
		GPIO_disableInterrupt(
				GPIO_PORT_P1,
				GPIO_PIN5
		);
	}else{
		frame_Finished++;
	}
    //P2.7 IFG cleared
    GPIO_clearInterrupt(
    		 GPIO_PORT_P1,
         GPIO_PIN5
         );
}

//******************************************************************************
//
//This is the PORT1_VECTOR interrupt vector service routine
//
//******************************************************************************
/*#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=PORT3_VECTOR
__interrupt
#elif defined(__GNUC__)
__attribute__((interrupt(PORT3_VECTOR)))
#endif
void Port_3(void)
{

	//trigger DMA here
	//Start block tranfer on DMA channel 0
	if(line_Start == 1){
			//each_line_bytes_Num++;
		DMA_startTransfer(DMA_CHANNEL_0);
	}
    //P3.6 IFG cleared
    GPIO_clearInterrupt(
    		GPIO_PORT_P3,
        GPIO_PIN6
        );
}*/


//DMA ISR
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=DMA_VECTOR
__interrupt
#elif defined(__GNUC__)
__attribute__((interrupt(DMA_VECTOR)))
#endif
void DMA_ISR(void) //after the DMA0SZ counter counts to 0, this interrupt happens
{
	P3OUT &= ~(0x04); //turn it off at the end of the frame we are taking data of
	//DMAIV &= ~(0x02); //clear the flag
	DMA0CTL &= ~(DMAIE); //interrupt disable
	//DMA0CTL &= ~(DMAEN); //disable DMA0
	//enable interrupt
	//frame_Start=0;
	//frame_Finished=0;
	frame_Finished++;
	//P2_2_interrupt_Conf();
	P1_5_interrupt_Conf();
}
/*
void toggle_PWDN() {
	P3DIR |= 0x04;                          // Set P3.2 to output direction
	P3OUT |= 0x04;
	__delay_cycles(100);
	P3OUT &= ~(0x04);
}
*/
