//polling version
#include <msp430.h>

/*freq: 24 ==> 24MHz
  freq: 16 ==> 16MHz
  freq: 8 ==> 8MHz
*/
unsigned char data_Recieved = '0';
unsigned char camAddr_R = 0x43;
unsigned char camAddr_W = 0x21;

void DCO_Conf(int freq)
{
    //SET CLOCK SURCE
    //CSCTL0_H = CSKEY;
    CSCTL0_H = 0xA5;       // Unlock CS registers
    switch(freq){
        case 24:
            CSCTL1_L = (DCOFSEL_6 | DCORSEL); //set DCO frequency 24 MHz
            FRCTL0 = 0xA500 | ((1) << 4);  //FRCTLPW | NWAITS_1;
            break;
        case 8:
            CSCTL1_L = DCOFSEL_6;
            CSCTL1_L &= ~(DCORSEL); //set DCO frequency 8 MHz
            break;
        case 16:
            CSCTL1_L = DCOFSEL_4; //DCOFSEL_4 == 0x8
            CSCTL1_L |= DCORSEL; //set DCO frequency 16 MHz
            FRCTL0 = 0xA500 | ((1) << 4);  //FRCTLPW | NWAITS_1;
            break;
    }
    CSCTL2_L = (0b0110011); //set the source of MCLK and SMCLK to be DCO
    CSCTL3_L = DIVS__1;        //SMCLK/1 and MCLK/1 Page:
    //CSCTL3_L = 0x0010; //SMCLK/2 and MCLK/1
    //CSCTL3_L = 0x0020; //SMCLK/4 and MCLK/1

    CSCTL4_L &= 0b01;    //SMCLK ON

    //SET I/O port Pin27 port3.4
    //try 10b ==> secondeary function
    P3DIR |= 0x010; // Set P3.4 to output direction

    //try tertiary
    P3SEL0 |= 0x0010; //P3SEL0.4 = 1
    P3SEL1 |= 0x0010;//P3SEL1.4 = 0
}


void i2cWriteToReg(const unsigned char byteAddr, const unsigned char data)
{
	UCB0I2CSA = camAddr_W; 								// set slave address
	while(UCB0CTLW0 & UCTXSTP);
	UCB0CTLW0 |= UCTR | UCTXSTT;							// put in transmitter mode and send start bit

	while(UCB0CTLW0 & UCTXSTT);
	while (!(UCB0IFG & UCTXIFG0)); 							// wait for previous tx to complete

	UCB0TXBUF = byteAddr;	 // setting TXBUF clears the TXIFG flag
	while (!(UCB0IFG & UCTXIFG0));

	UCB0TXBUF = data; 										// setting TXBUF clears the TXIFG flag
	while (!(UCB0IFG & UCTXIFG0)); 							// wait for previous tx to complete

	UCB0CTLW0 |= UCTXSTP; 									// I2C stop condition
	while (UCB0CTLW0 & UCTXSTP);
}

unsigned char i2cReadFromReg(const unsigned char byteAddr)
{
	unsigned char data = 0;

	/* **** WRITING TO CAMERA WRITE ADDRESS TO LOOK FOR REG **** */
	UCB0I2CSA = camAddr_W; 									// set slave address

	UCB0CTLW0 |= UCTR | UCTXSTT;							// put in transmitter mode and send start bit
	while (!(UCB0IFG & UCTXIFG0) | (UCB0CTLW0 & UCTXSTT)); 	// wait for previous tx to complete

	UCB0TXBUF = byteAddr; 									// setting TXBUF clears the TXIFG flag
	while (!(UCB0IFG & UCTXIFG0) | (UCB0CTLW0 & UCTXSTT)); 	// wait for previous tx to complete

	UCB0CTLW0 |= UCTXSTP;
	while (UCB0CTLW0 & UCTXSTP);


	/* **** WRITING TO CAMERA READ ADDRESS TO RECEIVE FROM REG **** */

	UCB0CTLW0 &= ~UCTR;										//change to receiver mode

	UCB0CTLW0 |= UCTXSTT;									// send start bit

	while (!(UCB0IFG & UCRXIFG0));							// wait for receive flag

	UCB0CTLW0 |= UCTXSTP; 									// I2C stop condition

	data = UCB0RXBUF;										// get the data

	while (UCB0CTLW0 & UCTXSTP);							// wait until stopping

	return data;
}

void i2c_init()
{
	P1SEL1 |= BIT6 | BIT7;                         			// Assign I2C pins to SCL/SDA
	P1SEL0 &= ~(BIT6 | BIT7);								// Selection mode P1SEL1=1 and P1SEL0=0, so mode 0b10 => SCL/SDA

	UCB0CTLW0 |= UCSWRST;                      				// Enable SW reset (reset state machine)

	UCB0CTLW0 |= UCMST | UCMODE_3;     						// I2C Master, synchronous mode
	UCB0CTLW0 |= UCSSEL_3;            						// Use SMCLK, keep SW reset
	//UCB0BRW = 0x1E;                             				// fSCL = SMCLK/120, for example
	UCB0BRW = 0x78; // /120
	UCB0CTLW0 &= ~UCSWRST;                    				// Clear SW reset, resume operation

	//UCB0IE |= UCNACKIE ;//| UCTXIE0 | UCRXIE0;			        // Enable TX and RX interrupt
	//UCB0IE |= UCNACKIE;//| UCTXIE0 | UCRXIE0;
	 __delay_cycles(5000000);
}

int main()
{
	WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer
	PM5CTL0 &= ~LOCKLPM5;

	DCO_Conf(24);
	i2c_init();
	//i2cWriteToReg(0x01, 0x80);
	i2cWriteToReg(0x01, 0x00);
	//i2cWriteToReg(0x11, 0b00011111);
	//data_Recieved = i2cReadFromReg(0x11);
	while(1){

	}
	return 0;
}