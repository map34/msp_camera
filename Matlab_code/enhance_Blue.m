%{
This functions take a RGB NxMx3 matrix and and enhance_Val to every pixel
of the blue channel
%}
function enhanced_RGB = enhance_Blue(RGB, enhance_Val)
    Blue = RGB(:, :, 3);
    enhancedBlue = max(Blue + enhance_Val, 1.0);  % Or whatever
    %enhancedBlue = max(Blue * 2, 1.0);
    RGB(:, :, 3) = enhancedBlue;
    enhanced_RGB = RGB;
    Title = 'RGB with enhanced blue';
    figure,imshow(RGB);title(Title); 
end