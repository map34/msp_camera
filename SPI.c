/* --COPYRIGHT--,BSD_EX
 * Copyright (c) 2012, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *******************************************************************************
 *
 *                       MSP430 CODE EXAMPLE DISCLAIMER
 *
 * MSP430 code examples are self-contained low-level programs that typically
 * demonstrate a single peripheral function or device feature in a highly
 * concise manner. For this the code may rely on the device's power-on default
 * register values and settings such as the clock configuration and care must
 * be taken when combining code from several examples to avoid potential side
 * effects. Also see www.ti.com/grace for a GUI- and www.ti.com/msp430ware
 * for an API functional library-approach to peripheral configuration.
 *
 * --/COPYRIGHT--*/
//******************************************************************************
//   MSP430F59xx Demo - eUSCI_A0, SPI 3-Wire Slave Data Echo
//
//   Description: SPI slave talks to SPI master using 3-wire mode. Data received
//   from master is echoed back.
//   ACLK = 32.768kHz, MCLK = SMCLK = DCO ~ 1MHz
//   Note: Ensure slave is powered up before master to prevent delays due to
//   slave init.
//
//
//                   MSP430FR5969
//                 -----------------
//            /|\ |              XIN|-
//             |  |                 |  32KHz Crystal
//             ---|RST          XOUT|-
//                |                 |
//                |             P2.0|<- Data In (UCA0SIMO) pin24
//                |                 |
//                |             P2.1|-> Data Out (UCA0SOMI) pin25
//                |                 |
//                |             P1.5|<- Serial Clock In (UCA0CLK) pin11
//
//								P1.4<- Slave Select (UCA0STE) pin10
//
//
//   P. Thanigai
//   Texas Instruments Inc.
//   Feb 2012
//   Built with CCS V5.5
//******************************************************************************
#include <msp430.h>


/*freq: 24 ==> 24MHz
  freq: 16 ==> 16MHz
  freq: 8 ==> 8MHz
*/
void DCO_Conf(int freq)
{
    //SET CLOCK SURCE
    //CSCTL0_H = CSKEY;
    CSCTL0_H = 0xA5;       // Unlock CS registers
    switch(freq){
        case 24:
            CSCTL1_L = (DCOFSEL_6 | DCORSEL); //set DCO frequency 24 MHz
            FRCTL0 = 0xA500 | ((1) << 4);  //FRCTLPW | NWAITS_1;
            break;
        case 8:
            CSCTL1_L = DCOFSEL_6;
            CSCTL1_L &= ~(DCORSEL); //set DCO frequency 8 MHz
            break;
        case 16:
            CSCTL1_L = DCOFSEL_4; //DCOFSEL_4 == 0x8
            CSCTL1_L |= DCORSEL; //set DCO frequency 16 MHz
            FRCTL0 = 0xA500 | ((1) << 4);  //FRCTLPW | NWAITS_1;
            break;
    }
    CSCTL2_L = (0b0110011); //set the source of MCLK and SMCLK to be DCO
    CSCTL3_L = DIVS__1;        //SMCLK/1 and MCLK/1 Page:
    //CSCTL3_L = 0x0010; //SMCLK/2 and MCLK/1
    //CSCTL3_L = 0x0020; //SMCLK/4 and MCLK/1

    CSCTL4_L &= 0b01;    //SMCLK ON

    //SET I/O port Pin27 port3.4
    //try 10b ==> secondeary function
    P3DIR |= 0x010; // Set P3.4 to output direction

    //try tertiary
    P3SEL0 |= 0x0010; //P3SEL0.4 = 1
    P3SEL1 |= 0x0010;//P3SEL1.4 = 0
}

void general_Init()
{
    WDTCTL = WDTPW | WDTHOLD;               // Stop watchdog timer
    //PM5CTL0 &= ~LOCKLPM5;                   // Disable the GPIO power-on default high-impedance mode
                                            // to activate previously configured port settings
	//P1SEL1 |= BIT0;
	//P1SEL0 &= ~(BIT0); //pinmux p1.0 to be DMA trigger pin
}

int main(void)
{
  //WDTCTL = WDTPW | WDTHOLD;                 // Stop watchdog timer
  general_Init(); //disable the LM5 mode and watch dog timer

  DCO_Conf(24); //configure the clock to MCLK to be 24MHz

  // Configure GPIO
  P1SEL1 |= BIT5 | BIT4;//UCA0CLK & SS                  // Configure SPI pins
  P2SEL1 |= BIT0 | BIT1; //SIMO & SOMI

  //PJSEL0 |= BIT4 | BIT5;                    // For XT1

  // Disable the GPIO power-on default high-impedance mode to activate
  // previously configured port settings
  PM5CTL0 &= ~LOCKLPM5;

  // Configure USCI_A0 for SPI operation
  UCA0CTLW0 = UCSWRST;                      // **Put state machine in reset**
  //sample code
  //UCSTEM

  //UCA0CTLW0 |= UCSYNC | UCCKPL | UCMSB;     // 3-pin, 8-bit SPI slave
  //UCA0CTLW0 |= (UCSYNC | UCCKPL | UCMSB | UCMODE_2)<<8; //| UCSTEM;
  // Clock polarity high, MSB
  //UCA0CTLW0 |= (UCSYNC | UCMODE_2 | UCMSB)<<8;
  UCA0CTLW0 |= (UCSYNC | UCMODE_2 | UCMSB);
  //UCA0CTLW0 |= UCSSEL__SMCLK;               // ACLK
  //UCA0BR0 = 0x02;                           // /2
  //UCA0BR1 = 0;                              //
  UCA0MCTLW = 0;                            // No modulation*/
  //my code
  /*UCA0CTLW0 |= 0x02; //bit 1 UCSTEM = 1 pin is used to generate the enable signal for a 4-wire slave
  UCA0CTLW0 |= 0x0100; //bit 8 UCSYNC = 1 Synchronous mode
  UCA0CTLW0 |= 0x0400; //bit 10 4-pin SPI with UCxSTE active low: Slave enabled when UCxSTE = 0
  UCA0CTLW0 &= ~(0x0800); //bit 11 0b = Slave mode
  UCA0CTLW0 &= ~(0x1000); //bit 12 0b = 8-bit data
  UCA0CTLW0 |= 0x2000; //bit 13 1b = MSB first
  UCA0CTLW0 |= 0x4000; //bit 14 1b = The inactive state is high.
  UCA0MCTLW = 0;                            // No modulation*/
  UCA0CTLW0 &= ~UCSWRST;                    // **Initialize USCI state machine**
  UCA0IE |= UCRXIE;                         // Enable USCI_A0 RX interrupt

  //__bis_SR_register(LPM0_bits | GIE);       // Enter LPM0, enable interrupts
  __bis_SR_register(GIE);
  while(1){

  }
}

#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=USCI_A0_VECTOR
__interrupt void USCI_A0_ISR(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(USCI_A0_VECTOR))) USCI_A0_ISR (void)
#else
#error Compiler not supported!
#endif
{
  while (!(UCA0IFG&UCTXIFG));               // USCI_A0 TX buffer ready?
  UCA0TXBUF = UCA0RXBUF;                    // Echo received data
}

